# Tugas Rumah Tutorial GIT

1. Clone project ini
2. Buat **branch** baru dengan nama `tugas-nama`. Contoh: `tugas-eka`
3. Edit file `Tugas Rumah GIT.txt` dengan menjelaskan istilah istilah git yang sudah disediakan dengan bahasa sendiri
4. Tugas Rumah menggunakan bahasa Indonesia
5. **Commit** perubahan file dengan pesan "nama - edit Tugas Rumah GIT". Contoh: "Eka - edit Tugas Rumah GIT"
6. Untuk pengumpulan menggunakan fitur **merge request** ke branch `master`
7. Pengumpulan paling lambat **Kamis, 5 November 2020 10.00 WIB**

> Jika ada pertanyaan bisa melalui chanel slack #magang-smkn13-2020 dan tag ekapratama, Dzikri Azzakah Asshiam, atau Fahrul Fauzi 

### Refferences
* Checkout new branch
```
$ git checkout -b *Name*
```
* Commit changes
```
$ git commit -m "*Message*"
```
* [Full cheatsheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)